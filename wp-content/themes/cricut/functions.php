<?php

/**
 * Functions and definitions
 */



// Exit-if accessed directly
if (!defined('ABSPATH'))
    exit;

/* THEME OPTIONS PAGE - HEADER, FOOTER, FAQS, CONSULTATION */
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(
        array(
            'page_title' => 'Theme Options',
            'menu_title' => 'Theme Options',
            'menu_slug' => 'theme-options',
            'capability' => 'edit_posts',
            'parent_slug' => '',
            'position' => false,
            'icon_url' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Header',
            'menu_title' => 'Header',
            'menu_slug' => 'theme-options-header',
            'capability' => 'edit_posts',
            'parent_slug' => 'theme-options',
            'position' => false,
            'icon_url' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Footer',
            'menu_title' => 'Footer',
            'menu_slug' => 'theme-options-footer',
            'capability' => 'edit_posts',
            'parent_slug' => 'theme-options',
            'position' => false,
            'icon_url' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Error',
            'menu_title' => 'Error',
            'menu_slug' => 'theme-options-error',
            'capability' => 'edit_posts',
            'parent_slug' => 'theme-options',
            'position' => false,
            'icon_url' => false,
            'post_id' => 'error',
        )
    );

    // acf_add_options_sub_page(
    //     array(
    //         'page_title' => 'Common Sections',
    //         'menu_title' => 'Common Sections',
    //         'menu_slug' => 'theme-options-common',
    //         'capability' => 'edit_posts',
    //         'parent_slug' => 'theme-options',
    //         'position' => false,
    //         'icon_url' => false
    //     )
    // );
}

/* ADD THEME SCRIPTS AND STYLE FILE */

function add_theme_scripts()
{

    /* Styles */
    wp_enqueue_style('font-style', get_template_directory_uri() . '/assets/css/font.css', false, '1.0', 'all');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/fontawesome.min.css', false, '1.0', 'all');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', false, '1.0', 'all');
    wp_enqueue_style('slick-style', get_template_directory_uri() . '/assets/css/slick.css', false, '1.0', 'all');
    wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/css/slick-theme.css', false, '1.0', 'all');
    wp_enqueue_style('common-style', get_template_directory_uri() . '/assets/css/common.css', false, '1.0', 'all');
    wp_enqueue_style('header-style', get_template_directory_uri() . '/assets/css/header.css', false, '1.0', 'all');

    if (is_page_template('front-page.php')) {
        wp_enqueue_style('homepage-style', get_template_directory_uri() . '/assets/css/homepage.css', false, '1.0', 'all');
    }

    if (taxonomy_exists('product_cat')) {
        wp_enqueue_style('category-page-style', get_template_directory_uri() . '/assets/css/category-page.css', false, '1.0', 'all');
    }

    if (is_singular('product')) {
        wp_enqueue_style('product-detail-page-style', get_template_directory_uri() . '/assets/css/product-detail-page.css', false, '1.0', 'all');
    }

    if (is_page('cart-page.php') || is_cart()) {
        wp_enqueue_style('cart-page-style', get_template_directory_uri() . '/assets/css/cart-page.css', false, '1.0', 'all');
    }

    if (is_page('checkout-page.php') || is_checkout()) {
        wp_enqueue_style('checkout-page-style', get_template_directory_uri() . '/assets/css/checkout-page.css', false, '1.0', 'all');
    }

    if (is_page('thankyou-page.php') || is_checkout()) {
        wp_enqueue_style('thankyou-page-style', get_template_directory_uri() . '/assets/css/thankyou-page.css', false, '1.0', 'all');
    }

    if (is_page_template('privacy-policy.php') || is_page_template('user-manual.php')) {
        wp_enqueue_style('privacy-policy-page-style', get_template_directory_uri() . '/assets/css/privacy-policy-page.css', false, '1.0', 'all');
    }

    if (is_account_page()) {
        wp_enqueue_style('form-page-style', get_template_directory_uri() . '/assets/css/form-page.css', false, '1.0', 'all');
        wp_enqueue_style('my-account-page-style', get_template_directory_uri() . '/assets/css/my-account-page.css', false, '1.0', 'all');
        wp_enqueue_style('lost-psw-page-style', get_template_directory_uri() . '/assets/css/lost-psw-page.css', false, '1.0', 'all');
    }

    wp_enqueue_style('footer-style', get_template_directory_uri() . '/assets/css/footer.css', false, '1.0', 'all');






    /* Scripts */

    if (!is_admin()) {
        //Call JQuery
        //wp_deregister_script('jquery');
    }
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery.js', array(), null, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.js', array(), null, true);
    // wp_enqueue_script('bootstrap-min', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), null, true);
    // wp_enqueue_script('bootstrap-bundle', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array(), null, true);
    wp_enqueue_script('popper', get_template_directory_uri() . '/assets/js/popper.min.js', array(), null, true);
    wp_enqueue_script('waypoints', get_template_directory_uri() . '/assets/js/waypoints.js', array(), null, true);
    wp_enqueue_script('slick-min', get_template_directory_uri() . '/assets/js/slick.min.js', array(), null, true);
    wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js', array(), null, true);
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

// For woocommerce support in custom theme 
function mytheme_add_woocommerce_support()
{
    add_theme_support('woocommerce');
}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');

/* REGISTER PRIMARY MENUS */
add_action('after_setup_theme', 'register_primary_menu');
function register_primary_menu()
{
    register_nav_menu('Links1', __('Links1 Menu', 'cricut'));
    register_nav_menu('Links2', __('Links2 Menu', 'cricut'));
    register_nav_menu('Links3', __('Links3 Menu', 'cricut'));
    register_nav_menu('Header', __('Header Mega Menu', 'cricut'));
}

/* change input to quantity plus and minus increment */

add_action('woocommerce_after_add_to_cart_quantity', 'ts_quantity_plus_sign');

function ts_quantity_plus_sign()
{
    echo '<button type="button" class="plus" >+</button>';
}

add_action('woocommerce_before_add_to_cart_quantity', 'ts_quantity_minus_sign');

function ts_quantity_minus_sign()
{
    echo '<button type="button" class="minus" >-</button>';
}

add_action('wp_footer', 'ts_quantity_plus_minus');

function ts_quantity_plus_minus()
{
    // To run this on the single product page
    if (!is_product()) return;
?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            $('form.cart').on('click', 'button.plus, button.minus', function() {

                // Get current quantity values
                var qty = $(this).closest('form.cart').find('.qty');
                var val = parseFloat(qty.val());
                var max = parseFloat(qty.attr('max'));
                var min = parseFloat(qty.attr('min'));
                var step = parseFloat(qty.attr('step'));

                // Change the value if plus or minus
                if ($(this).is('.plus')) {
                    if (max && (max <= val)) {
                        qty.val(max);
                    } else {
                        qty.val(val + step);
                    }
                } else {
                    if (min && (min >= val)) {
                        qty.val(min);
                    } else if (val > 1) {
                        qty.val(val - step);
                    }
                }

            });

        });
    </script>
<?php
}

/* Add Product Page Section */
add_action('woocommerce_after_add_to_cart_form', 'product_description_faq', 40);
function product_description_faq()
{
    if (is_product()) {
        get_template_part('template-parts/product-description-faq');
    }
}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 5);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 15);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 10);

/* Add Product Page Section ------ Bundle Section */
add_action('woocommerce_after_single_product', 'other_product_page_section', 5);

// product description tab replace
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
// add_action('woocommerce_after_single_product', 'custom_fake_tab', 10);
function custom_fake_tab()
{
?>
    <div class="woocommerce-tabs wc-tabs-wrapper">
    </div>
<?php
}
add_action('woocommerce_after_single_product', 'woocommerce_output_product_data_tabs', 40);

// results count hook
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
add_action('woocommerce_before_main_content', 'woocommerce_result_count', 20);

function other_product_page_section()
{
    if (is_product()) {
        get_template_part('template-parts/bundle-section');
        get_template_part('template-parts/product-video-section');
        get_template_part('template-parts/product-banner-section');
        get_template_part('template-parts/product-slider-section');
        get_template_part('template-parts/product-grid-section');
        get_template_part('template-parts/product-highlight-item-section');
        get_template_part('template-parts/product-app-section');
        get_template_part('template-parts/product-faq-section');
    }
}


/**
 * Create Shortcode for WooCommerce Cart Menu Item
 */
add_shortcode('woo_cart_but', 'woo_cart_but');
function woo_cart_but()
{
    ob_start();

    // $cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
    $cart_count = count(WC()->cart->get_cart());
    //print_r(WC()->cart);
    //die;
    $cart_url = wc_get_cart_url();  // Set Cart URL

?>
    <a class="menu-item cart-contents" href="<?php echo $cart_url; ?>" title="My Basket">
        <!-- <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.06312 0H0.0767386C-0.291689 1.52384 0.822678 3 2.34133 3H4.42786L8.05345 16.5L19.2964 13.4544C21.2211 12.9329 22.1833 12.6723 22.7773 11.9389C23.3712 11.2054 23.4505 10.1798 23.6091 8.12861L24.0059 3H8.05274L7.87672 2.27239C7.6131 1.18109 7.48119 0.635303 7.08769 0.317679C6.69438 1.60933e-05 6.15053 1.594e-05 5.06285 1.594e-05L5.06312 0Z" fill="black" />
            <path d="M8.41633 24C9.81799 24 10.9542 22.8247 10.9542 21.375C10.9542 19.9253 9.81799 18.75 8.41633 18.75C7.01468 18.75 5.87842 19.9253 5.87842 21.375C5.87842 22.8247 7.01468 24 8.41633 24Z" fill="black" />
            <path d="M21.8307 21.375C21.8307 22.8247 20.6945 24 19.2928 24C17.8911 24 16.7549 22.8247 16.7549 21.375C16.7549 19.9253 17.8911 18.75 19.2928 18.75C20.6945 18.75 21.8307 19.9253 21.8307 21.375Z" fill="black" />
        </svg> -->
        <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0_14_346)">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M16.4649 12.273C16.6165 12.2766 16.7646 12.2276 16.8841 12.1342C17.0035 12.0409 17.0869 11.909 17.1199 11.761L18.2009 6.091H6.72191L7.85591 11.759C7.91591 12.063 8.16791 12.237 8.49291 12.273H16.4649ZM19.4099 4.09C19.557 4.09038 19.7023 4.12315 19.8353 4.186C19.9683 4.24884 20.0859 4.34021 20.1796 4.45361C20.2734 4.56701 20.341 4.69966 20.3777 4.84214C20.4144 4.98462 20.4192 5.13343 20.3919 5.278L19.0829 12.142C18.8329 13.402 17.7259 14.277 16.4459 14.272H8.46291C7.85539 14.2693 7.26753 14.0564 6.79922 13.6694C6.33091 13.2823 6.01102 12.7451 5.89391 12.149L4.52791 5.324C4.5221 5.30024 4.51743 5.27621 4.51391 5.252L3.86291 2H1.40991C1.1447 2 0.890342 1.89464 0.702805 1.70711C0.515269 1.51957 0.409912 1.26522 0.409912 1C0.409912 0.734784 0.515269 0.48043 0.702805 0.292893C0.890342 0.105357 1.1447 7.85099e-08 1.40991 7.85099e-08H4.68191C4.91318 -9.15226e-05 5.13732 0.0799759 5.31619 0.22657C5.49506 0.373164 5.61758 0.577222 5.66291 0.804L6.32091 4.091L19.4099 4.09ZM7.94991 15.545C7.46791 15.5455 7.0058 15.7372 6.66498 16.0781C6.32415 16.4189 6.13244 16.881 6.13191 17.363C6.13218 17.8452 6.32377 18.3075 6.66462 18.6486C7.00548 18.9896 7.46774 19.1815 7.94991 19.182C8.43226 19.1815 8.89467 18.9895 9.23555 18.6482C9.57644 18.307 9.76791 17.8443 9.76791 17.362C9.76712 16.8802 9.57529 16.4183 9.2345 16.0777C8.8937 15.7371 8.43174 15.5455 7.94991 15.545ZM16.9549 15.545C16.4854 15.5641 16.0415 15.764 15.716 16.103C15.3905 16.4419 15.2088 16.8936 15.2088 17.3635C15.2088 17.8334 15.3905 18.2851 15.716 18.624C16.0415 18.963 16.4854 19.1629 16.9549 19.182C17.4244 19.1629 17.8684 18.963 18.1938 18.624C18.5193 18.2851 18.701 17.8334 18.701 17.3635C18.701 16.8936 18.5193 16.4419 18.1938 16.103C17.8684 15.764 17.4244 15.5641 16.9549 15.545Z" fill="#6B6B6B" />
            </g>
            <defs>
                <clipPath id="clip0_14_346">
                    <rect width="20" height="20" fill="white" transform="translate(0.409912)" />
                </clipPath>
            </defs>
        </svg>
        <?php
        if ($cart_count > 0) {
        ?>
            <span class="cart-contents-count"><?php echo $cart_count; ?></span>
        <?php
        }
        ?>
        <span>Cart</span></a>
<?php

    return ob_get_clean();
}


add_filter('woocommerce_add_to_cart_fragments', 'woo_cart_but_count');
/**
 * Add AJAX Shortcode when cart contents update
 */
function woo_cart_but_count($fragments)
{

    ob_start();

    // $cart_count = WC()->cart->cart_contents_count;
    $cart_count = count(WC()->cart->get_cart());
    $cart_url = wc_get_cart_url();

?>
    <a class="cart-contents menu-item" href="<?php echo $cart_url; ?>" title="<?php _e('View your shopping cart'); ?>">
        <!-- <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.06312 0H0.0767386C-0.291689 1.52384 0.822678 3 2.34133 3H4.42786L8.05345 16.5L19.2964 13.4544C21.2211 12.9329 22.1833 12.6723 22.7773 11.9389C23.3712 11.2054 23.4505 10.1798 23.6091 8.12861L24.0059 3H8.05274L7.87672 2.27239C7.6131 1.18109 7.48119 0.635303 7.08769 0.317679C6.69438 1.60933e-05 6.15053 1.594e-05 5.06285 1.594e-05L5.06312 0Z" fill="black" />
            <path d="M8.41633 24C9.81799 24 10.9542 22.8247 10.9542 21.375C10.9542 19.9253 9.81799 18.75 8.41633 18.75C7.01468 18.75 5.87842 19.9253 5.87842 21.375C5.87842 22.8247 7.01468 24 8.41633 24Z" fill="black" />
            <path d="M21.8307 21.375C21.8307 22.8247 20.6945 24 19.2928 24C17.8911 24 16.7549 22.8247 16.7549 21.375C16.7549 19.9253 17.8911 18.75 19.2928 18.75C20.6945 18.75 21.8307 19.9253 21.8307 21.375Z" fill="black" />
        </svg> -->
        <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0_14_346)">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M16.4649 12.273C16.6165 12.2766 16.7646 12.2276 16.8841 12.1342C17.0035 12.0409 17.0869 11.909 17.1199 11.761L18.2009 6.091H6.72191L7.85591 11.759C7.91591 12.063 8.16791 12.237 8.49291 12.273H16.4649ZM19.4099 4.09C19.557 4.09038 19.7023 4.12315 19.8353 4.186C19.9683 4.24884 20.0859 4.34021 20.1796 4.45361C20.2734 4.56701 20.341 4.69966 20.3777 4.84214C20.4144 4.98462 20.4192 5.13343 20.3919 5.278L19.0829 12.142C18.8329 13.402 17.7259 14.277 16.4459 14.272H8.46291C7.85539 14.2693 7.26753 14.0564 6.79922 13.6694C6.33091 13.2823 6.01102 12.7451 5.89391 12.149L4.52791 5.324C4.5221 5.30024 4.51743 5.27621 4.51391 5.252L3.86291 2H1.40991C1.1447 2 0.890342 1.89464 0.702805 1.70711C0.515269 1.51957 0.409912 1.26522 0.409912 1C0.409912 0.734784 0.515269 0.48043 0.702805 0.292893C0.890342 0.105357 1.1447 7.85099e-08 1.40991 7.85099e-08H4.68191C4.91318 -9.15226e-05 5.13732 0.0799759 5.31619 0.22657C5.49506 0.373164 5.61758 0.577222 5.66291 0.804L6.32091 4.091L19.4099 4.09ZM7.94991 15.545C7.46791 15.5455 7.0058 15.7372 6.66498 16.0781C6.32415 16.4189 6.13244 16.881 6.13191 17.363C6.13218 17.8452 6.32377 18.3075 6.66462 18.6486C7.00548 18.9896 7.46774 19.1815 7.94991 19.182C8.43226 19.1815 8.89467 18.9895 9.23555 18.6482C9.57644 18.307 9.76791 17.8443 9.76791 17.362C9.76712 16.8802 9.57529 16.4183 9.2345 16.0777C8.8937 15.7371 8.43174 15.5455 7.94991 15.545ZM16.9549 15.545C16.4854 15.5641 16.0415 15.764 15.716 16.103C15.3905 16.4419 15.2088 16.8936 15.2088 17.3635C15.2088 17.8334 15.3905 18.2851 15.716 18.624C16.0415 18.963 16.4854 19.1629 16.9549 19.182C17.4244 19.1629 17.8684 18.963 18.1938 18.624C18.5193 18.2851 18.701 17.8334 18.701 17.3635C18.701 16.8936 18.5193 16.4419 18.1938 16.103C17.8684 15.764 17.4244 15.5641 16.9549 15.545Z" fill="#6B6B6B" />
            </g>
            <defs>
                <clipPath id="clip0_14_346">
                    <rect width="20" height="20" fill="white" transform="translate(0.409912)" />
                </clipPath>
            </defs>
        </svg>
        <?php
        if ($cart_count > 0) {
        ?>
            <span class="cart-contents-count"><?php echo $cart_count; ?></span>
        <?php
        }
        ?><span>Cart</span></a>
<?php

    $fragments['a.cart-contents'] = ob_get_clean();

    return $fragments;
}


// To allow svg file upload in admin
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/* change dropdown into radio buttons product detail page */

add_action('woocommerce_before_shop_loop_item_title', 'add_hover_image_on_product_thumbnail', 15);
function add_hover_image_on_product_thumbnail()
{
    global $product;
    $product_hover_image = get_field('hover_image');

    echo '<img src="' . $product_hover_image . '" class="thumbnail-hover-img" alt="product-hover-img">';
}

/* upselles product */

remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

add_action('woocommerce_after_single_product', 'woocommerce_upsell_display', 5);
add_action('woocommerce_after_single_product', 'woocommerce_output_related_products', 5);
