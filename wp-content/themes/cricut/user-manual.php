<?php

/***
 * Template Name: User Manual Page Template
 */
get_header();
?>

<?php if (have_rows('user_manual_content')) : ?>
    <?php while (have_rows('user_manual_content')) : the_row(); ?>
        <div class="privacy-policy-banner">
            <picture>
                <?php if (get_sub_field('mobile_image')) : ?>
                    <source media="(max-width:650px)" srcset="<?php echo get_sub_field('mobile_image'); ?>" alt="Image Mobile">
                <?php endif; ?>

                <?php if (get_sub_field('desktop_image')) : ?>
                    <source media="(max-width:1500px)" srcset="<?php echo get_sub_field('desktop_image'); ?>" alt="Image Mobile">
                <?php endif; ?>

                <?php if (get_sub_field('large_desktop_image') != "") : ?>
                    <img src="<?php echo get_sub_field('large_desktop_image'); ?>" alt="Image" style="width:100%;">
                <?php endif; ?>
            </picture>
            <div class="container">
                <div class="row">
                    <dv class="col-12">
                        <h2>User Manual</h2>
                    </dv>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('user_manual_content')) : ?>
    <?php while (have_rows('user_manual_content')) : the_row(); ?>
        <section class="privacy-policy-data">
            <div class="container">
                <div class="row">
                    <?php if (have_rows('user_manual')) : ?>
                        <h4>Documentation</h4>
                        <div class="col-lg-3 col-md-4 col-12">
                            <?php $i = 1; ?>
                            <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <?php while (have_rows('user_manual')) : the_row(); ?>
                                    <button class="nav-link <?php if ($i == 1) : ?>active <?php endif; ?>" id="v-pills-home-tab<?php echo $i; ?>" data-bs-toggle="pill" data-bs-target="#v-pills-home<?php echo $i; ?>" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true"><span><?php echo get_sub_field('heading'); ?></span></button>
                                    <?php $i++; ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-8 col-12">
                            <?php $i = 1; ?>
                            <div class="tab-content" id="v-pills-tabContent">
                                <?php while (have_rows('user_manual')) : the_row(); ?>
                                    <div class="tab-pane fade <?php if ($i == 1) : ?>show active <?php endif; ?>" id="v-pills-home<?php echo $i; ?>" role="tabpanel" aria-labelledby="v-pills-home-tab<?php echo $i; ?>">
                                        <?php echo get_sub_field('content'); ?>
                                    </div>
                                    <?php $i++; ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();
?>