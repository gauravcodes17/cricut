<!---------- Footer Section ---------->
<footer>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-12">
                <div class="row justify-content-between">
                    <div class="col-lg-5 col-md-6">
                        <div class="row">
                            <div class="col-lg-5 col-md-6">
                                <h6><?php echo get_field('heading1', 'option'); ?></h6>
                                <?php
                                // Links1 Menu
                                if (function_exists('register_primary_menu')) :
                                    wp_nav_menu(array(
                                        'theme_location' => 'Links1',
                                    ));
                                endif;
                                ?>
                            </div>
                            <div class="col-lg-7 col-md-6">
                                <h6><?php echo get_field('heading2', 'option'); ?></h6>
                                <?php
                                // Links1 Menu
                                if (function_exists('register_primary_menu')) :
                                    wp_nav_menu(array(
                                        'theme_location' => 'Links2',
                                    ));
                                endif;
                                ?>

                                <h6 class="last"><?php echo get_field('heading3', 'option'); ?></h6>
                                <?php
                                // Links1 Menu
                                if (function_exists('register_primary_menu')) :
                                    wp_nav_menu(array(
                                        'theme_location' => 'Links3',
                                    ));
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <h4><?php echo get_field('tagline', 'option'); ?></h4>
                        <?php
                        $link = get_field('link', 'option');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a class="primary_button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                        <h5><a href="tel:<?php echo get_field('number', 'option'); ?>"><?php echo get_field('contact_us', 'option'); ?></a></h5>
                        <div class="footer-icons">
                            <?php $social_links = get_field('social_icons', 'option'); ?>

                            <?php if ($social_links) : ?>
                                <?php foreach ($social_links as $social) : ?>
                                    <a href="<?php echo $social['url']; ?>" target="_blank">
                                        <?php echo $social['icons']; ?>
                                    </a>
                                <?php endforeach; ?>
                            <?php endif; ?>

                        </div>
                        <div class="country-selector">
                            <a href="<?php echo get_field('country_selector_url', 'option'); ?>">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 40 39" style="enable-background:new 0 0 40 39" xml:space="preserve">
                                    <style>
                                        .st0 {
                                            fill: #fff
                                        }

                                        .st3 {
                                            fill: #008
                                        }
                                    </style>
                                    <circle class="st0" cx="20" cy="19.4" r="19.3" />
                                    <path d="M20 .1C11.7.1 4.6 5.4 1.8 12.7h36.3C35.4 5.4 28.3.1 20 .1z" style="fill:#f93" />
                                    <path d="M20 38.9c8.3 0 15.4-5.3 18.2-12.6H1.8c2.8 7.3 9.9 12.6 18.2 12.6z" style="fill:#128807" />
                                    <g>
                                        <circle class="st3" cx="20" cy="19.5" r="5.9" />
                                        <circle class="st0" cx="20" cy="19.5" r="5.2" />
                                        <circle class="st3" cx="20" cy="19.5" r="1" />
                                        <path class="st3" d="M25.1 19.9c-.1 0-.2 0-.3.2v.1c0 .2 0 .2.3.2.1 0 .3-.1.4-.2 0-.2-.1-.3-.4-.3.1 0 .1 0 0 0zM19.8 21.6l.2 3.1.2-3.1-.2-1.5zM24.9 21.2c-.1-.1-.3-.1-.4 0-.1.1 0 .3.1.4.1.1.3 0 .4-.1 0-.1-.1-.2-.1-.3zM19.3 21.5l-.6 3 .9-3 .3-1.4zM24.2 22.4c-.1-.1-.3-.1-.4 0-.1.1-.1.3 0 .4.1.1.3.1.4 0v-.1c.2 0 .2-.2 0-.3.1 0 0 0 0 0zM19.7 20l-.9 1.2-1.4 2.8 1.7-2.6zM23 23.4c-.1.1-.1.2 0 .3.1.1.2.1.2 0 .1-.1.2-.2.2-.4-.1-.1-.2-.2-.4-.1v.2zM19.6 19.9l-1.2.9-2 2.3 2.3-2zM21.9 24c-.1.1-.2.2-.2.3.1.1.2.2.4.2.1-.1.2-.2.2-.4-.1-.1-.3-.2-.4-.1zM19.5 19.8l-1.4.6-2.6 1.7 2.8-1.4z" />
                                        <circle class="st3" cx="20.7" cy="24.6" r=".3" />
                                        <path class="st3" d="m18 20.2 1.4-.6-1.4.3-3 .9zM19.4 24.4c-.2 0-.2.1-.2.3 0 .1.1.2.2.2.2 0 .3-.1.3-.3-.2-.1-.3-.2-.3-.2zM17.9 19.3l-3.1.2 3.1.2 1.5-.2zM18.1 24c-.1-.1-.3-.1-.4 0-.1.1 0 .3.1.4.1.1.3 0 .4-.1.1-.1 0-.2-.1-.3zM18 18.8l-3-.6 3 .9 1.4.3zM17 23.4c-.1-.1-.3 0-.4.2-.1.1-.1.2 0 .2.1.1.3.1.4-.1.1-.1.1-.3 0-.3zM19.5 19.2l-1.2-.9-2.8-1.4 2.6 1.7zM15.8 22.4c-.1.1-.1.3 0 .4.1.1.2.1.3 0v-.2c0-.2-.2-.2-.3-.2zM19.6 19.1l-.9-1.2-2.3-2 2 2.3zM15.1 21.2c-.1.1-.2.2-.2.4.1.1.2.2.4.2.1-.1.2-.2.2-.4s-.2-.2-.4-.2c.1 0 .1 0 0 0zM19.7 19l-.6-1.4-1.7-2.6 1.4 2.8z" />
                                        <circle class="st3" cx="14.9" cy="20.2" r=".3" />
                                        <path class="st3" d="m19.9 18.9-.3-1.4-.9-3 .6 3zM14.9 19.1c.1 0 .2 0 .3-.2v-.1c0-.2-.1-.2-.3-.2-.1 0-.2.1-.2.2-.1.1 0 .3.2.3zM20.2 17.4l-.2-3.1-.2 3.1.2 1.5zM15.1 17.8c.1.1.3.1.4 0 .1-.1.1-.3 0-.4-.1-.1-.3-.1-.4 0 0 .1-.1.3 0 .4zM20.7 17.5l.6-3-.9 3-.3 1.4zM15.8 16.6c.1.1.3.1.4 0 .1-.1.1-.3-.1-.4-.1-.1-.2-.1-.3 0-.2.1-.2.3 0 .4-.1 0 0 0 0 0zM20.3 19l.9-1.2 1.4-2.8-1.7 2.6zM17 15.6c.1-.1.1-.2 0-.3-.1-.1-.2-.1-.2 0-.1.1-.1.2 0 .3.1.1.1.1.2 0zM20.4 19.1l1.2-.9 2-2.3-2.3 2zM18.1 15c.1-.1.2-.2.2-.3-.1-.1-.2-.2-.4-.2-.1.1-.2.2-.2.4.1.1.3.2.4.1zM20.5 19.2l1.4-.6 2.6-1.7-2.8 1.4z" />
                                        <circle class="st3" cx="19.3" cy="14.4" r=".3" />
                                        <path class="st3" d="m25 18.2-3 .6-1.4.6 1.4-.3zM20.6 14.6c.2 0 .2 0 .2-.3 0-.1-.1-.2-.2-.2-.2 0-.3.1-.3.3.2.1.3.2.3.2zM22.1 19.3l-1.5.2 1.5.2 3.1-.2zM21.9 15c.1.1.3.1.4 0 .1-.1 0-.3-.1-.4-.1-.1-.3 0-.4.1-.1.1 0 .2.1.3zM22 20.2l3 .6-3-.9-1.4-.3zM23 15.6c.1.1.3 0 .4-.2.1-.1.1-.2 0-.2-.1-.1-.3-.1-.4.1-.1.1-.1.3 0 .3zM20.5 19.8l1.2.9 2.8 1.4-2.6-1.7zM24.2 16.6c.1-.1.1-.3 0-.4-.1-.1-.2-.1-.3 0-.1.1-.1.2 0 .2 0 .2.1.3.3.2zM20.4 19.9l.9 1.2 2.3 2-2-2.3zM24.9 17.8c.1-.1.2-.2.2-.4-.1-.1-.2-.2-.4-.2-.1.1-.2.2-.2.4s.2.2.4.2c-.1 0-.1 0 0 0zM20.3 20l.6 1.4 1.7 2.6-1.4-2.8z" />
                                        <circle class="st3" cx="25.1" cy="18.8" r=".3" />
                                        <path class="st3" d="m20.1 20.1.3 1.4.9 3-.6-3z" />
                                    </g>
                                </svg> India - English <svg width="12" height="12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11.192 1.404a1 1 0 00-1-1h-9a1 1 0 100 2h8v8a1 1 0 102 0v-9zm-9.485 9.9L10.9 2.11 9.486.697.293 9.889l1.414 1.414z" fill="#121212"></path>
                                </svg></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-between">
                    <div class="col-lg-5 col-md-6">
                        <div class="footer-copyrights">
                            <p><?php echo get_field('copyright', 'option'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <div class="footer-address">
                            <p><?php echo get_field('address', 'option'); ?></p>
                        </div>
                    </div>
                </div>
                <?php if (get_field('footer_content', 'option') != "") : ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="footer-policies-details">
                                <p><?php echo get_field('footer_content', 'option'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>

<?php
wp_footer();
?>

</body>

</html>