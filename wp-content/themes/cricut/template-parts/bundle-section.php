<?php if (have_rows('bundle_section')) : ?>
    <?php while (have_rows('bundle_section')) : the_row(); ?>
        <?php if (get_sub_field('heading') != "") : ?>
            <div class="product-bundle-section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h4><?php echo get_sub_field('heading'); ?></h4>
                        </div>
                    </div>
                    <div class="row">
                        <?php if (have_rows('bundle_item')) : ?>
                            <?php
                            // Loop through cols.
                            while (have_rows('bundle_item')) : the_row();
                            ?>
                                <div class="col-lg-3 col-md-6 col-6">
                                    <img src="<?php echo get_sub_field('item_image'); ?>" class="img-fluid" alt="">
                                    <p><?php echo get_sub_field('item_title'); ?></p>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>