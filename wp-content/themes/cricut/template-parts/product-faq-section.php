<?php if (have_rows('faq_section')) : ?>
    <?php while (have_rows('faq_section')) : the_row(); ?>
        <?php if (get_sub_field('heading') != "") : ?>
            <div class="product-faq-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-12">
                            <h4><?php echo get_sub_field('heading'); ?></h4>
                        </div>
                        <div class="col-lg-8 col-12">
                            <div class="accordion" id="accordionExample2">
                                <?php
                                // Loop through rows.
                                $i = 1;
                                while (have_rows('faq')) : the_row();
                                ?>
                                    <div class="accordion-item">
                                        <h5 class="accordion-header" id="heading<?php echo $i; ?>">
                                            <button class="accordion-button collapsed<?php if ($i != 1) : ?> collapsed <?php endif; ?>" type="button" data-bs-toggle="collapse" data-bs-target="#collapsee<?php echo $i; ?>" aria-expanded="<?php if ($i != 1) : ?> false <?php else : ?> true <?php endif; ?>" aria-controls="collapse<?php echo $i; ?>">
                                                <?php echo get_sub_field('question'); ?>
                                            </button>
                                        </h5>
                                        <div id="collapsee<?php echo $i; ?>" class="accordion-collapse collapse <?php if ($i == 1) : ?> <?php endif; ?>" aria-labelledby="heading<?php echo $i; ?>" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                <?php echo get_sub_field('answer'); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                    $i++;
                                // End loop.
                                endwhile;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>