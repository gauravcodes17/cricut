<?php if (have_rows('items_section')) : ?>
    <div class="product-highlight-item-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul>
                        <?php while (have_rows('items_section')) : the_row(); ?>
                            <li>
                                <?php echo get_sub_field('item_icon'); ?>
                                <p><?php echo get_sub_field('item_text'); ?></p>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>