<?php if (have_rows('product_description_faq')) : ?>
    <div class="products-description-faq">
        <div class="accordion" id="accordionExample">
            <!-- <h4><?php //echo get_field('faq_heading'); ?></h4> -->
            <?php
            // Loop through rows.
            $i = 1;
            while (have_rows('product_description_faq')) : the_row();
            ?>
                <div class="accordion-item">
                    <h5 class="accordion-header" id="heading<?php echo $i; ?>">
                        <button class="accordion-button collapsed<?php if ($i != 1) : ?> collapsed <?php endif; ?>" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $i; ?>" aria-expanded="<?php if ($i != 1) : ?> false <?php else : ?> true <?php endif; ?>" aria-controls="collapse<?php echo $i; ?>">
                            <?php echo get_sub_field('question'); ?>
                        </button>
                    </h5>
                    <div id="collapse<?php echo $i; ?>" class="accordion-collapse collapse <?php if ($i == 1) : ?> show <?php endif; ?>" aria-labelledby="heading<?php echo $i; ?>" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <?php echo get_sub_field('answer'); ?>
                        </div>
                    </div>
                </div>
            <?php
                $i++;
            // End loop.
            endwhile;
            ?>
        </div>
    </div>
<?php endif; ?>



<div class="product-details-faq d-none">
    <div class="accordion accordion-flush" id="accordionFlushExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingOne">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                    Accordion Item #1
                </button>
            </h2>
            <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                    Accordion Item #2
                </button>
            </h2>
            <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the second item's accordion body. Let's imagine this being filled with some actual content.</div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                    Accordion Item #3
                </button>
            </h2>
            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
            </div>
        </div>
    </div>
</div>