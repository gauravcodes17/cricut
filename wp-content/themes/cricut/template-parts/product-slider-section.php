<?php if (have_rows('slider_section')) : ?>
    <?php while (have_rows('slider_section')) : the_row(); ?>
        <?php if (get_sub_field('heading') != "") : ?>
            <div class="product-slider-section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h4><?php echo get_sub_field('heading'); ?></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="projectGallery-slides">
                                <?php if (have_rows('slider')) : ?>
                                    <?php while (have_rows('slider')) : the_row(); ?>
                                        <div class="pg-inner-slides">
                                            <picture>
                                                <?php if (get_sub_field('mobile_image')) : ?>
                                                    <source media="(max-width:991px)" srcset="<?php echo get_sub_field('mobile_image'); ?>" alt="Image Mobile">
                                                <?php endif; ?>

                                                <img src="<?php echo get_sub_field('desktop_image'); ?>" alt="Image" style="width:100%;">
                                            </picture>
                                            <h6><?php echo get_sub_field('text'); ?></h6>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                            <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                <span class="slider__label sr-only">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>