<?php if (have_rows('easy_to_learn_app_section')) : ?>
    <?php while (have_rows('easy_to_learn_app_section')) : the_row(); ?>
        <?php if (get_sub_field('large_desktop_image') != "") : ?>
            <div class="product-app-section">
                <div class="app-background-image">
                    <picture>
                        <?php if (get_sub_field('mobile_image')) : ?>
                            <source media="(max-width:650px)" srcset="<?php echo get_sub_field('mobile_image'); ?>" alt="Image Mobile">
                        <?php endif; ?>

                        <?php if (get_sub_field('desktop_image')) : ?>
                            <source media="(max-width:1500px)" srcset="<?php echo get_sub_field('desktop_image'); ?>" alt="Image Mobile">
                        <?php endif; ?>

                        <?php if (get_sub_field('large_desktop_image') != "") : ?>
                            <img src="<?php echo get_sub_field('large_desktop_image'); ?>" alt="Image" style="width:100%;">
                        <?php endif; ?>
                    </picture>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="app-content">
                                <div>
                                    <h4><?php echo get_sub_field('heading'); ?></h4>
                                    <p><?php echo get_sub_field('text'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>