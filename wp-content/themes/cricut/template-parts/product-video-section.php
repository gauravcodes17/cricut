<?php if (have_rows('video_section')) : ?>
    <?php while (have_rows('video_section')) : the_row(); ?>
        <?php if (get_sub_field('video_link') != "") : ?>
            <div class="product-video-section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="y-video">
                                <iframe width="100" height="100" src="<?php echo get_sub_field('video_link'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>