<?php if (have_rows('customers_gallery')) : ?>
    <?php while (have_rows('customers_gallery')) : the_row(); ?>
        <?php if (get_sub_field('heading') != "") : ?>
            <div class="product-grid-section">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-12">
                            <h4><?php echo get_sub_field('heading'); ?></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="grid-m">
                                <?php if (have_rows('gallery')) : ?>
                                    <?php while (have_rows('gallery')) : the_row(); ?>
                                        <img src="<?php echo get_sub_field('image'); ?>" class="" alt="gallery">
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-12">
                            <button class="primary_button" id="load-more-gallery">Load More</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>