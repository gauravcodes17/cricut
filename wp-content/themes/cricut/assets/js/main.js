console.log("start");

// !(function () {
//   "use strict";
//   const e = {
//     init: function () {
//       let e;
//       $(".experience-slidesContainer:not(.slick-initialized)").each(
//         function () {
//           let i = $(this);
//           i.slick({
//             arrows: !0,
//             prevArrow: i.siblings(".projectGallery-previous"),
//             nextArrow: i.siblings(".projectGallery-next"),
//             centerMode: !0,
//             infinite: !1,
//             slidesToShow: 1.3,
//           }),
//             i.on("beforeChange", function (i, n, t, r) {
//               e = (function (e, i, n) {
//                 n && clearTimeout(n);
//                 let t = ((i + 1) / e.$slides.length) * 100;
//                 return (
//                   e.$slider
//                     .parent()
//                     .siblings(".scrollbar-container")
//                     .animate({ opacity: 1 }, 150),
//                   e.$slider
//                     .parent()
//                     .siblings(".scrollbar-container")
//                     .children(".scrollbar")
//                     .animate({ width: t + "%" }, 500),
//                   setTimeout(function () {
//                     e.$slider
//                       .parent()
//                       .siblings(".scrollbar-container")
//                       .animate({ opacity: 0 }, 500);
//                   }, 3e3)
//                 );
//               })(n, r, e);
//             });
//         }
//       );
//     },
//   };
//   window.addEventListener("DOMContentLoaded", e.init),
//     window.addEventListener("pdContentLoaded", e.init);
// })();

(function ($) {
  $(document).ready(function () {
    $(".single-product .woocommerce-breadcrumb").addClass("container");

    if ($(window).width() < 767) {
      $("footer h6").click(function () {
        $(this).toggleClass("open");
        $(this).next().toggle(200);
      });
    }

    var $slider = $(".projectGallery-slides");
    var $progressBar = $(".progress");
    var $progressBarLabel = $(".slider__label");

    $slider.on(
      "beforeChange",
      function (event, slick, currentSlide, nextSlide) {
        var calc = (nextSlide / (slick.slideCount - 1)) * 100;

        $progressBar
          .css("background-size", calc + "% 100%")
          .attr("aria-valuenow", calc);

        $progressBarLabel.text(calc + "% completed");
      }
    );

    $slider.slick({
      arrows: true,
      dots: false,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 400,
      centerMode: true,
      centerPadding: "300px",
      // prevArrow:
      //   '<button class="slick-arrow slick-prev"><i class="fa-solid fa-arrow-right-long"></i></button>',
      // nextArrow:
      //   '<button class="slick-arrow slick-next"><i class="fa-solid fa-arrow-right-long"></i></button>',
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1,
            centerPadding: "150px",
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            centerPadding: "100px",
          },
        },
      ],
    });
  });

  $("#load-more-gallery").click(function () {
    console.log("hello");
    $(".product-grid-section .grid-m img").show(100);
    $(this).css("display", "none");
  });

  // $(".products-description-faq .accordion-item h5 button").click(function() {
  //   console.log("clicked..........");
  //     var item = $(this)
  //       .parent("h5.accordion-header")
  //       .siblings("div");

  //       console.log(item);

  //       if(item.hasClass("show")){
  //         $(item).removeClass("show");
  //         console.log("kkkkkkkkkk")
  //       }
  // });

  /* product page variations product */

  // function setColorName() {
  //   $("ul.variable-items-wrapper li.variable-item").each(function () {
  //     var title = $(this).attr("title");

  //     $(this)
  //       .find("div.variable-item-contents")
  //       .css("background-color", title);
  //     // console.log(title);
  //     let cont = $(this).find("div span.variable-item-span");

  //     if ($(this).hasClass("selected") == true) {
  //       $(cont).text("");
  //       // console.log(title);
  //     } else {
  //       $(cont).text(title);
  //     }
  //   });
  // }

  // setColorName();

  // $("ul.variable-items-wrapper li.variable-item").click(function () {
  //   // console.log("hello");
  //   let clickItem = $(this).attr("title");
  //   // console.log(clickItem);

  //   $("li.variable-item").each(function () {
  //     var title = $(this).attr("title");
  //     // console.log(title);
  //     let cont = $(this).find("div span.variable-item-span");

  //     if (title == clickItem) {
  //       $(cont).text("");
  //       // console.log(title);
  //     } else {
  //       $(cont).text(title);
  //     }
  //   });
  // });
  
})(jQuery);
