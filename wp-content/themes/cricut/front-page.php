<?php

/***
 * Template Name: Home Page Template
 */
get_header();
?>

<!---------- Main Banner Section ---------->
<?php if (have_rows('creative_banner')) : ?>
    <?php while (have_rows('creative_banner')) : the_row(); ?>
        <div class="creative-banner-section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 gx-0">
                        <picture>
                            <?php if (get_sub_field('mobile_image')) : ?>
                                <source media="(max-width:650px)" srcset="<?php echo get_sub_field('mobile_image'); ?>" alt="Image Mobile">
                            <?php endif; ?>

                            <?php if (get_sub_field('desktop_image') != "") : ?>
                                <img src="<?php echo get_sub_field('desktop_image'); ?>" alt="Image" style="width:100%;">
                            <?php endif; ?>
                        </picture>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>

<!---------- Main Trending Banner Section ---------->
<?php if (have_rows('trending_collections')) : ?>
    <section class="trending-banner-section">
        <div class="container">
            <div class="row">
                <?php while (have_rows('trending_collections')) : the_row(); ?>
                    <div class="col-md-4 col-12">
                        <img src="<?php echo get_sub_field('image') ?>" class="img-fluid" alt="">
                        <h5><?php echo get_sub_field('heading'); ?></h5>
                        <p><?php echo get_sub_field('text'); ?></p>
                        <div class="button-div">
                            <?php
                            $link = get_sub_field('link');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a class="primary_button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<!---------- Shop By Category Section ---------->
<?php if (have_rows('shop_by_category')) : ?>
    <div class="shop-by-category">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h4>Shop by Category</h4>
                </div>
            </div>
            <div class="row text-center justify-content-between">
                <?php while (have_rows('shop_by_category')) : the_row(); ?>
                    <div class="col-lg-2 col-md-4 col-6">
                        <a href="#">
                            <picture>
                                <source media="(max-width:650px)" srcset="<?php echo get_sub_field('icon'); ?>" alt="">
                                <img src="<?php echo get_sub_field('icon') ?>" alt="">
                            </picture>
                        </a>
                        <p><?php echo get_sub_field('text'); ?></p>
                    </div>
                <?php endwhile ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<!---------- Warranty Banner Section ---------->
<?php //if (have_rows('warranty_section')) : 
?>
<?php //while (have_rows('warranty_section')) : the_row(); 
?>
<!-- <section class="warranty-banner">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12">
                        <h4><?php //echo get_sub_field('heading'); 
                            ?></h4>
                        <?php /*
                        $link = get_sub_field('link');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';*/
                        ?>
                            <a class="secondary_button" href="<?php //echo esc_url($link_url); 
                                                                ?>" target="<?php //echo esc_attr($link_target); 
                                                                            ?>"><?php //echo esc_html($link_title); 
                                                                                ?></a>
                        <?php //endif; 
                        ?>
                    </div>
                </div>
            </div>
        </section> -->
<?php //endwhile; 
?>
<?php //endif; 
?>

<!---------- Video Section ---------->
<?php //if (have_rows('video_section')) : ?>
    <section class="video-section">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <video width="100%" autoplay muted loop playsinline>
                        <source src="<?php echo get_field('video_section'); ?>" type="video/mp4">
                        <source src="<?php echo get_field('video_section'); ?>" type="video/ogg">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
        </div>
    </section>
<?php //endif; ?>

<!---------- Bundle Section ---------->
<?php if (have_rows('bundle_section')) : ?>
    <section class="bundle-section">
        <div class="container">
            <?php while (have_rows('bundle_section')) : the_row(); ?>
                <div class="row justify-content-around">
                    <div class="col-lg-5">
                        <div>
                            <h4><?php echo get_sub_field('heading'); ?></h4>
                            <p><?php echo get_sub_field('text'); ?></p>
                            <?php
                            $link = get_sub_field('link');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a class="tertiary_button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <picture>
                            <source media="(max-width:650px)" srcset="<?php echo get_sub_field('image'); ?>" alt="">
                            <img src="<?php echo get_sub_field('image') ?>" class="img-fluid" alt="">
                        </picture>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </section>
<?php endif; ?>

<!---------- Virtual Demo Section ---------->
<?php if (have_rows('virtual_demo_section')) : ?>
    <?php while (have_rows('virtual_demo_section')) : the_row(); ?>
        <section class="virtual-demo">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="vd-content">
                            <h4><?php echo get_sub_field('heading'); ?></h4>
                            <?php if (have_rows('point')) : ?>
                                <ul>
                                    <?php while (have_rows('point')) : the_row(); ?>
                                        <li><?php echo get_sub_field('text'); ?></li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                            <?php
                            $link = get_sub_field('link');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a class="secondary_button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 gx-0">
                        <picture>
                            <source media="(max-width:650px)" srcset="<?php echo get_sub_field('image'); ?>" alt="">
                            <img src="<?php echo get_sub_field('image') ?>" class="img-fluid" alt="">
                        </picture>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();
?>