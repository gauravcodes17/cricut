<?php

/***
 * Template Name: Error Page Template
 */
get_header();
?>

<section class="error-page">
    <picture>
        <?php if (get_field('mobile_image', 'error') != "") : ?>
            <source media="(max-width:650px)" srcset="<?php echo get_field('mobile_image', 'error'); ?>" alt="Image Mobile">
        <?php endif; ?>

        <?php if (get_field('desktop_image', 'error') != "") : ?>
            <source media="(max-width:1500px)" srcset="<?php echo get_field('desktop_image', 'error'); ?>" alt="Image Mobile">
        <?php endif; ?>

        <?php if (get_field('large_desktop_image', 'error') != "") : ?>
            <img src="<?php echo get_field('large_desktop_image', 'error'); ?>" alt="Image" style="width:100%;">
        <?php endif; ?>
    </picture>
</section>

<div class="error-page-content">
    <div class="container">
        <div class="row text-center">
            <div class="col-12">
                <h4><?php echo get_field('heading', 'error'); ?></h4>
                <p><?php echo get_field('text', 'error'); ?></p>
            </div>
        </div>
        <div class="row text-center mt-4">
            <div class="col-12">
                <?php
                $link = get_field('link_1', 'error');
                if ($link) :
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                    <a class="error-btn err-mr" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                <?php endif; ?>
                <?php
                $link = get_field('link_2', 'error');
                if ($link) :
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                    <a class="error-btn2" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>