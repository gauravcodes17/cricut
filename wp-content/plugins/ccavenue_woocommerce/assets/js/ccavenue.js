var ccavenue = (function () {
return {
    isTouchDevice: function() {
        return 'ontouchstart' in window        // works on most browsers 
            || navigator.maxTouchPoints;       // works on IE10/11 and Surface
        }
    };
})();

var ccavenueIframe = (function () {
    return {
        showPaymentPage: function(form) {
            if(jQuery("#ccavenue_iframe").size()) {
                jQuery( "#ccavenue_iframe" ).remove();
            }
            jQuery('.ccav-iframe-container').append(form);
            jQuery('.woocommerce-terms-and-conditions-wrapper').remove();
            jQuery('button#place_order').remove();
            
            //fix for touch devices
            if (ccavenue.isTouchDevice()) {
                setTimeout(function() {
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                }, 1);
            }
        },
        iframeLoaded: function(){
            jQuery('#ccavenue_iframe').show();
        },
    };
})();