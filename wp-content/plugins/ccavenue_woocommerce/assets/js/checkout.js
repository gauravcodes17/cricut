

jQuery('form.checkout').on('submit', function (e){
    var paymentMethod = jQuery('input[name=payment_method]:checked').val();
    if("ccavenue" === paymentMethod) {
        e.preventDefault();
        return ccavenueFormHandler(jQuery(this));
    }
});
jQuery('form#order_review').on('submit', function () {
    var paymentMethod = jQuery('input[name=payment_method]:checked').val();
    if("ccavenue" === paymentMethod) {
        e.preventDefault();
        return ccavenueFormHandler(jQuery(this));
    }
});

function showError(form, data) {
    // Remove notices from all sources
    jQuery( '.woocommerce-NoticeGroup-checkout, .woocommerce-error, .woocommerce-message' ).remove();

    // Add new errors returned by this event
    if ( data.messages ) {
        form.prepend( '<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout">' + data.messages + '</div>' );
    } else {
        form.prepend( data );
    }

    // Lose focus for all fields
    form.find( '.input-text, select, input:checkbox' ).trigger( 'validate' ).trigger( 'blur' );

    // Scroll to top
    jQuery( 'html, body' ).animate( {
        scrollTop: ( jQuery( form ).offset().top - 100 )
    }, 1000 );
}

var form = jQuery("form.checkout");
form.length ? (form.bind("checkout_place_order_ccavenue", function() {
    //return ccavenueFormHandler(jQuery(this));
    return !1;
})) : jQuery("form#order_review").submit(function() {
    var paymentMethod = jQuery("#order_review input[name=payment_method]:checked").val();
    return "ccavenue" === paymentMethod ? ccavenueFormHandler(jQuery(this)) : void 0;
});

function ccavenueFormHandler(form) {
    if (form.is(".processing")) return !1;
    return initCcavenuePayment(form);
}

function isIframeMethod(pament_method) {
    var isCc = pament_method == 'ccavenue' ? true : false;
    if(isCc && jQuery('#ccavenue_integration_type').val() == 'iframe') {
        return true;
    }
    return false;
}

function isRedirectMethod(pament_method) {
    var isCc = pament_method == 'ccavenue' ? true : false;
    if(isCc && jQuery('#ccavenue_integration_type').val() == 'redirect') {
        return true;
    }
    return false;
}

function initCcavenuePayment(form) {
    var data = jQuery(form).serialize();
    var pament_method = form.find('input[name="payment_method"]:checked').val();
    var ajaxUrl = wc_checkout_params.checkout_url;
    jQuery.ajax({
        'url': ajaxUrl,
        'type': 'POST',
        'dataType': 'json',
        'data': data,
        'async': false
    }).always(function (response) {
        data = '';
        if(response.responseText) {
            var code = response.responseText;
            var newstring = code.replace(/<script[^>]*>(.*)<\/script>/, "");
            if (newstring.indexOf("<!--WC_START-->") >= 0) {
                newstring = newstring.split("<!--WC_START-->")[1];
            }
            if (newstring.indexOf("<!--WC_END-->") >= 0) {
                newstring = newstring.split("<!--WC_END-->")[0];
            }
            try {
                data = jQuery.parseJSON( newstring );
            }
            catch(e) {}
        } else {
            data = response;
        }
        if(data.result == 'failure') {
            showError(form, data);
            return !1;
        }
        if (data.form) {
            window.success = true;
            if(isRedirectMethod(pament_method)) {
                jQuery('#frm_ccavenue_payment').remove();
                jQuery('body').append(data.form);
            }
            else if(isIframeMethod(pament_method)) {
                ccavenueIframe.showPaymentPage(data.form);
            }
            else{                   
                jQuery( "#frm_ccavenue_payment" ).submit();
            }
        }
    });
    return !1;
}