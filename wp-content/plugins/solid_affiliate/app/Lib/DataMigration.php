<?php

namespace SolidAffiliate\Lib;

/**
 * This will serve as the starting point for migrating data into Solid Affiliate, 
 * namely from other plugins such as AffiliateWP.
 * 
 * Current Task:
 *   - Migrate 1 affiliate for a customer manually.
 *      - Affiliate has user ID 79
 *      - Affiliate has custom slug chuckosuagwu
 *      - Recurring referal rate 25%
 *      - lifetime commissions enabled
 *      - [x] auto coupon enabled (customer set this up already but we need to confirm it's working once we create the affiliate)
 * 
 */
class DataMigration
{
}