=== WPC Save For Later for WooCommerce ===
Contributors: wpclever
Donate link: https://wpclever.net
Tags: woocommerce, woo, save, later, wpc
Requires at least: 4.0
Tested up to: 6.3
Stable tag: 3.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A WordPress plugin for WooCommerce to add save for later functionality to products in your store. It will improve customer retention and return visits to your site by giving you.

== Description ==

There are times when customers need to save products for future consideration when they decide that they won’t need them for their current order yet. It’s impossible to save products when they are already added to the cart? Well, it’s possible with **WPC Save for Later**.

**WPC Save for Later for WooCommerce** will add a little but powerful button under each item on the cart page that allows buyers to save products for later with ease. These items will appear on the Saved for Later list right under the main items on the cart so that customers won’t forget to include them in their next order. Moving a product from the cart to the Save for Later list or vice versa only takes 1 click.

= Live demo =

Visit our [live demo](https://demo.wpclever.net/woosl/ "live demo") here to see how this plugin works.

= Features =

- Unlimited items can be added to the list
- Support AJAX for the Add to Cart button
- 1-click for adding products to cart from Save for Later or removing from the list
- 1-click for saving all products for later
- 1-click for adding all products to the cart
- Responsive display for desktop & mobile view
- Compatible with most common WordPress themes and plugins

= Translators =

Available Languages: English (Default), German

If you have created your own language pack, or have an update for an existing one, you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress") to [us](https://wpclever.net/contact?utm_source=pot&utm_medium=woosl&utm_campaign=wporg "WPClever.net") so we can bundle it into WPC Save For Later.

= Not what you needed? =

Please try other plugins from us:

- [WPC Product Bundles](https://wordpress.org/plugins/woo-product-bundle/ "WPC Product Bundles")
- [WPC Composite Products](https://wordpress.org/plugins/wpc-composite-products/ "WPC Composite Products")
- [WPC Grouped Product](https://wordpress.org/plugins/wpc-grouped-product/ "WPC Grouped Product")
- [WPC Frequently Bought Together](https://wordpress.org/plugins/woo-bought-together/ "WPC Frequently Bought Together")
- [WPC Force Sells](https://wordpress.org/plugins/wpc-force-sells/ "WPC Force Sells")

== Installation ==

1. Please make sure that you installed WooCommerce
2. Go to plugins in your dashboard and select "Add New"
3. Search for "WPC Save For Later", Install & Activate it
4. Now you can see the save for later functionality on the Cart page

== Changelog ==

= 3.1.0 =
* Added: "Save all for later" & "Add all to cart" button

= 3.0.0 =
* Updated: Save whole data of variation product
* Updated: Optimized the code

= 2.2.2 =
* Fixed: Minor CSS/JS issues in the backend

= 2.2.1 =
* Updated: Optimized the code

= 2.2.0 =
* Added: "Saved for later" page on My Account

= 2.1.2 =
* Fixed: Minor CSS/JS issues in the backend

= 2.1.1 =
* Added: HPOS compatibility

= 2.1.0 =
* Added: Option to choose the position for save-for-later list

= 2.0.1 =
* Added: Localization tab

= 2.0.0 =
* Updated: Compatible with WPC Fly Cart
* Updated: Don't reload the cart page
* Updated: Optimized the code

= 1.2.8 =
* Fixed: Table heading wasn't shown on the mobile device

= 1.2.7 =
* Fixed: Minor CSS/JS issues

= 1.2.6 =
* Updated: Optimized the code

= 1.2.5 =
* Added: Filter hook 'woosl_button'
* Fixed: Minor JS issue

= 1.2.4 =
* Updated: Optimized the code

= 1.2.3 =
* Updated: Compatible with WordPress 5.8 & WooCommerce 5.5.1

= 1.2.2 =
* Updated: Optimized the code

= 1.2.1 =
* Updated: Show saved products while having no products in the cart

= 1.2.0 =
* Updated: Compatible with WordPress 5.6 & WooCommerce 4.8

= 1.1.2 =
* Updated: Compatible with WooCommerce 4.6.1

= 1.1.1 =
* Updated: Optimized the code

= 1.1.0 =
* Updated: Compatible with WordPress 5.5 & WooCommerce 4.3.3

= 1.0.6 =
* Updated: Compatible with WordPress 5.4.2 & WooCommerce 4.2

= 1.0.5 =
* Updated: Compatible with WordPress 5.4 & WooCommerce 4.0.1

= 1.0.4 =
* Updated: Compatible with WordPress 5.3.2 & WooCommerce 3.9.3

= 1.0.3 =
* Updated: Optimized the code

= 1.0.2 =
* Updated: Compatible with WordPress 5.3 & WooCommerce 3.8

= 1.0.1 =
* Updated: Compatible with WooCommerce 3.6

= 1.0.0 =
* Released